﻿using System;

namespace lab02
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 26;  // a value type
            object a = i; //boxing
            int res = (int)a; //unboxing



            var d = new Digit(7);

            byte number = d;
            Console.WriteLine(number);  // output: 7

            Digit digit = (Digit)number;
            Console.WriteLine(digit);  // output: 7
        }



        public readonly struct Digit
        {
            private readonly byte digit;

            public Digit(byte digit)
            {
                if (digit > 9)
                {
                    throw new ArgumentOutOfRangeException(nameof(digit), "Digit cannot be greater than nine.");
                }
                this.digit = digit;
            }

            public static implicit operator byte(Digit d) => d.digit;
            public static explicit operator Digit(byte b) => new Digit(b);

            public override string ToString() => $"{digit}";
        }
    }
    abstract class Car
    {
        protected ICarIsDriving carIsDriving;
        public Car(ICarIsDriving carIsDriving)
        {
            this.carIsDriving = carIsDriving;
        }
        public string driving() { return carIsDriving.Drive(); }

    }

    interface ICarIsDriving
    {
        string Drive();
    }

    class EngineIsWorking : ICarIsDriving
    {
        string ICarIsDriving.Drive() { return "I'm working"; }
    }
    class EngineIsBroken : ICarIsDriving
    {
        string ICarIsDriving.Drive() { return "I'm broken. Please, change the oil! "; }
    }

    class Toyota : Car
    {
        public Toyota(ICarIsDriving carIsDriving) : base(carIsDriving) { }
    }



    class Showcase1
    {
        private int a1;
        public int a2;
        protected int a3;
        internal int a4;
        protected internal int a5;
        protected private int a6;
    }

    class Showcase2 : Showcase1
    {
        public void Set()
        {
            this.a1 = 1;
            this.a2 = 1;
            this.a3 = 1;
            this.a4 = 1;
            this.a5 = 1;
            this.a6 = 1;
        }
    }

    class Showcase3
    {
        protected Showcase1 showcase = new Showcase1();
        public void Set()
        {
            this.showcase.a1 = 1;
            this.showcase.a2 = 1;
            this.showcase.a3 = 1;
            this.showcase.a4 = 1;
            this.showcase.a5 = 1;
            this.showcase.a6 = 1;
        }
    }

    interface IShowcase1
    {
        private void a1() { } 
        public void a2() { }
        protected void a3() {  }
        internal void a4() {  }
        protected internal void a5() { }
        protected private void a6() { } 

        private void a7() { }   //read only
        void a8() { }
    }

    class IShowcase2:IShowcase1
    {
        private void a1() {}
        public void a2() {}
        protected void a3() {}
        internal void a4() {}
        protected internal void a5() {}
        protected private void a6() {}

        this.IShowcase1.a7 = 5;
        public void a8() { }
    }





    abstract class Showcase4
    {
        public abstract void a1();
        protected abstract void a2();
        internal abstract void a3();
        protected internal abstract void a4();
        protected private abstract void a5();

        private abstract void a6();
        abstract void a7();
    }

    class Showcase5:Showcase4
    {
        public override void a1() { }
        protected override void a2() { }
        internal override void a3() { }
        protected internal override void a4() { }
        protected private override void a5() { }

        private override void a6() { }
        override void a7() { }
    }


    class Showcase6  //internal
    {
        int a1; //private
        void a2() { } //private
        struct a3{ } //private
        class a4 { } //private

        enum a5 { }  // public
    }

    struct Showcase7  //internal
    {
        int a1;   //private
    }

    interface IShowcase8  //internal
    {
        void a1() { } //public
    }
    enum a { }  //internal



    enum Week { sunday, monday, tuesday, wednesday, thursday, frieday, saturday};

    class Showcase9
    {
        bool CheckWorkingDays(Week day)
        {
            if ( !(day == Week.saturday) && !(day == Week.sunday)) return true;  
            return false;
        }

        bool a1(Week day)
        {
            if (!(day == Week.sunday) | (day == Week.frieday) & (day == Week.monday^ day==Week.thursday)) return true;
            return false;
        }
    }


    interface IA1
    {
        void a1() { }
    }
    interface IA2
    {
        void a2() { }
    }

    class Showcase10 : IA1, IA2 { }
    interface IA3:IA1, IA2 { }




    class Base
    {
        protected int a;
        public Base(int a) { this.a = a; }

        void A(int a) {this.a}
        void A(int a1, int a2) { this.a = a1>a2?a1:a2}
    }
    class Showcase11:Base
    {
        public Showcase11(int a) : base(a) { }
    }


    class Showcase12
    {
        static int a1;

        static Showcase12()   //Static Constructor
        {
            a1 = 5;
        }

        public static Showcase12 New()  //Dynamic Constructor
        {
            return new Showcase12();
        }


    }

    class Showcase13
    {
        public void A1(ref int a, out int res)
        {
            res = ++a;
        }

        public int A2(ref int [] a, out int res)
        {
            res = ++a[^a.Length];
            return res;
        }
    }

    class Showcase14
    {
        private string a = "red";

        public override string ToString()
        {
            return a.ToString();
        }
        public override bool Equals(object obj)
        {
            return a.Equals(obj);
        }
        public override int GetHashCode()
        {
            return a.GetHashCode();
        }
    }
}
