﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

public interface ICallIntialization
{
    string[] callIntialization(int i);
}

public interface IA1 { }
public interface IA2 { }
public interface IA3 { }
public interface IA4 { }
public interface IA5 { }
public interface IA6 { }
public interface IA7 { }
public interface IA8 { }
public interface IA9 { }
public interface IA10 { }
class A1 { public A1() { } }
class A2 : IA1, IA2, IA3, IA4, IA5 { public A2() { } }
class A3 : IA1, IA2, IA3, IA4, IA5, IA6, IA7, IA8, IA9, IA10 { public A3() { } }

public interface IB1 { public void a1() { } }
public interface IB2 { public void a2() { } }
public interface IB3 { public void a3() { } }
public interface IB4 { public void a4() { } }
public interface IB5 { public void a5() { } }
public interface IB6 { public void a6() { } }
public interface IB7 { public void a7() { } }
public interface IB8 { public void a8() { } }
public interface IB9 { public void a9() { } }
public interface IB10 { public void a10() { } }

class B : IB1, IB2, IB3, IB4, IB5, IB6, IB7, IB8, IB9, IB10 {  
}

public interface IC1 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC2 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC3 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC4 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC5 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC6 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC7 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC8 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC9 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }
public interface IC10 { void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { } }

class C : IC1, IC2, IC3, IC4, IC5, IC6, IC7, IC8, IC9, IC10 { public C() { } }


public interface ID1 { 
    void a1() { } void a2() { } void a3() { } void a4() { } void a5() { } void a6() { } void a7() { } void a8() { } void a9() { } void a10() { }
    void a11() { } void a12() { } void a13() { } void a14() { } void a15() { } void a16() { } void a17() { }  void a18() { } void a19() { } void a20() { }}
public interface ID2
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID3
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID4
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID5
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID6
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID7
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID8
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID9
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
public interface ID10
{
    void a1() { }
    void a2() { }
    void a3() { }
    void a4() { }
    void a5() { }
    void a6() { }
    void a7() { }
    void a8() { }
    void a9() { }
    void a10() { }
    void a11() { }
    void a12() { }
    void a13() { }
    void a14() { }
    void a15() { }
    void a16() { }
    void a17() { }
    void a18() { }
    void a19() { }
    void a20() { }
}
class D : ID1, ID2, ID3, ID4, ID5, ID6, ID7, ID8, ID9, ID10 { public D() { } }

class Initialize 
{

    
    public string[] callIntializationA(int count)
    {

        Type staticClassInfo = typeof(InitStatic);

        var staticClassConstructorInfo = staticClassInfo.TypeInitializer;

        Stopwatch stopWatch = new Stopwatch();
        TimeSpan ts;
        string[] Results = new string[3];
        Results[0] = "Constructor Initialization(0 interface): ";
        Results[1] = "Constructor Initialization(5 interface): ";
        Results[2] = "Constructor Initialization(10 interface): ";
        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            A1 init = new A1();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[0] += Result(ts) + "\n";


        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            A2 init = new A2();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[1] += Result(ts) + "\n";


        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            A3 init = new A3();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[2] += Result(ts) + "\n";

        return Results;

    }
    
    public string[] callIntialization(int count)
    {

        Type staticClassInfo = typeof(InitStatic);

        var staticClassConstructorInfo = staticClassInfo.TypeInitializer;

        Stopwatch stopWatch = new Stopwatch();
        TimeSpan ts;
        string[] Results = new string[3];
        Results[0] = "1 field in interface: ";
        Results[1] = "10 field in interface: ";
        Results[2] = "20 field in interface: ";
        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            B temp = new B();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[0] += Result(ts) + "\n";


        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            C temp = new C();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[1] += Result(ts) + "\n";


        stopWatch.Start();
        for (int i = 0; i < count; i++)
        {

            D temp = new D();

        }
        stopWatch.Stop();
        ts = stopWatch.Elapsed;
        Results[2] += Result(ts) + "\n";

        return Results;

    }
    private string Result(TimeSpan ts)

    {

        return String.Format("{0:00}:{1:00}:{2:00}.{3:00}",

            ts.Hours, ts.Minutes, ts.Seconds,

            ts.Milliseconds / 10);

    }

}



static class InitStatic

{

    static int initStatic;

    static InitStatic()

    {

        var rand = new Random();

        initStatic = rand.Next();

    }

}




class lab02_2

{

    public static void Main()
    {

        Initialize classInit = new Initialize();

        string[] classRes = classInit.callIntializationA(100000000);

        for (int i = 0; i < classRes.Length; i++)
        {
            Console.WriteLine(classRes[i]);
        }
        classRes = classInit.callIntialization(100000000);

        for (int i = 0; i < classRes.Length; i++)
        {
            Console.WriteLine(classRes[i]);
        }

        Console.ReadLine();
    }

}