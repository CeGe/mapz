﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace lab03.Tests
{
    [TestClass]
    public class UnitTest1
    {
        ListOfBands k = new ListOfBands();
        [TestMethod]
        public void TestMethod1()
        {

            var ra = k.bands.Select(x => x).Where(x => x.numberOfFans > 10).ToList();
            var ar = k.FindBandsByFans(10);
            Assert.IsTrue(ra.SequenceEqual<Band>(ar));
        }
        [TestMethod]
        public void TestMethod2()
        {
            Dictionary<string, Band> Temp = new Dictionary<string, Band>();
            k.bands.Select(x => x)
                .Where(x => x.name.Length > 3)
                .ToList<Band>()
                .ForEach(x => Temp.Add(x.name, x));

        }
        [TestMethod]
        public void TestMethod3()
        {
            var temp1 = k.bands
            .GetCorrect()
            .Select(x => new
            {
                Name = x.name,
                Release = x.numberOfFans
            });

            string nameOfBand = "ToyWorld";
            var temp2 = new
            {
                name = nameOfBand,
                numberOfFans = 32
            };
        }


       




        public class BandComparer : IComparer<Band>
        {
            public int Compare(Band a, Band b)
            {
                if (a.name.Length == b.name.Length)
                    return 0;
                else if (a.name.Length < b.name.Length)
                    return -1;
                else
                    return 1;

            }

        }
        [TestMethod]
        public void TestMethod4()
        {
            k.bands.Sort(new BandComparer());
        }
        [TestMethod]
        public void TestMethod5()
        {
            var answ = k.FindBandsByFans(10)
            .Select(x => x.numberOfFans)
            .ToList()
            .ToArray();
            var sorted = answ.OrderByDescending(x => x).ToList().ToArray();
            Assert.IsTrue(answ.SequenceEqual(sorted));
        }
        [TestMethod]
        public void TestMethod6()
        {
            var temp1 = k.bands.OrderByDescending(x => x);

            var temp2 = k.bands.Select(x => x.name).ToList().OrderBy(x=>x);
            temp2.ForEach(x => Console.WriteLine(x));
        }
        [TestMethod]
        public void TestMethod7()
        {
            ListOfBands listOfBands = new ListOfBands();
            listOfBands.bands = (new F()).Bands;
            listOfBands.Output();
        }
        [TestMethod]
        public void TestMethod8()
        {
            ListOfBands listOfBands = new ListOfBands();
            listOfBands.bands = (new F()).Bands;
            var diction = listOfBands.bands.GetCorrect()
                    .ToDictionary(x => x.name, x=>x.listOFAlbums);

            SortedList<string, List<int>> slist = new SortedList<string, List<int>>();
            diction.ForEach(x => slist.Add(x.Key, x.Value));

            SortedDictionary<string, List<int>> sdiction = new SortedDictionary<string, List<int>>();
            diction.ForEach(x => sdiction.Add(x.Key, x.Value));

            Queue<Band> qbands = new Queue<Band>();
            listOfBands.bands.ForEach(x=>qbands.Enqueue(x));
            for (int i = 0; i < qbands.Count; i++)
                qbands.Dequeue().Output();




            var keyValuePairs = listOfBands.bands
                .Select(x=>x)
                .Where(x=> x.numberOfFans>10)
                .ToDictionary(y=>y, y=> { y.listOFAlbums.Sort(); return y.listOFAlbums; });

        }
        [TestMethod]
        public void TestMethod9()
        {
        }
    }


    public static class LinqExtensions
    {
        static public void ForEach<T>(this IEnumerable<T> arr, Action<T> action)
        {
            foreach (var item in arr)
            {
                action(item);
            }
        }
        static public IEnumerable<T> GetCorrect<T>(this IEnumerable<T> arr) where T : Band
        {
            return arr.Where(x => x.numberOfFans > 0)
            .Where(x => x.name != "");
            
        }
    }



   

        



}
