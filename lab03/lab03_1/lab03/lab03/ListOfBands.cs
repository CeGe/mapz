﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab03
{
    public class ListOfBands
    {
        public List<Band> bands = new List<Band>() { new Band(4, "red"), new Band(100, "Zwintar"), 
            new Band(6, "SadSvit"), new Band(54, "OE"), new Band(33, "BoomBox") };
        public ListOfBands() { }
        public List<Band> FindBandsByFans(int n)
        {
            List<Band> temp=new List<Band>();
            foreach(var band in bands)
            {
                if (band.numberOfFans > n) temp.Add(band);
            }

            return temp;
        }
        public void Output()
        {
            bands.ForEach(x => x.Output());
        }
    }

    public class Band
    {
        public int numberOfFans;  // one unit to a thousand
        public string name;
        public Band() { }
        public Band(int n, string name)
        {
            this.numberOfFans = n;
            this.name = name;
        }
        public List<int> listOFAlbums { get; set; }
        public static Band ParseFromCSV(string str)
        {
            Band band = new Band();
            string[] vs = str.Split(' ');

            if (vs.Length == 0) return band;
            band.name = vs[0];
            band.numberOfFans = Convert.ToInt32(vs[1]);
            band.listOFAlbums = new List<int>();
                string[] tempStr = vs[2].Split(',');
                List<int> temp= new List<int>();
                foreach (var t in tempStr) band.listOFAlbums.Add(Convert.ToInt32(t)) ;
            return band;
        }

        private string toString()
        {
            string str = "";
            listOFAlbums.ForEach(x => str += x.ToString() + "\n");
            return str;
        }
        public void Output()
        {
            string BandsInfo = string.Format("==============================\n" +
                "Band:{0}\n" +"Fans:{1}\n" + "{2}" 

                 + "==============================\n", this.name, this.numberOfFans, this.toString());
            Console.WriteLine(BandsInfo);
        }

        
    }

   

    public class F
    {
        public List<Band> Bands
        {
            get
            {
                return new List<Band>
                {
                    Band.ParseFromCSV("OE 356 7654,4323,1986"),
                    Band.ParseFromCSV("SadSvit 983 1834"),
                    Band.ParseFromCSV("BoomBox 332 2354,4653"),
                    Band.ParseFromCSV("TwentyOnePilots 102 9870,2133,6436,2653,9236")
                };
            }
        }
    }










}
